//function created to replace the content
function replaceContent() {
	//declaring the variables
	var myRecipientName; //Recipient
	var hostName; //Host
	
	//setting the variable to the input field's id named recipientNameInput's value
	myRecipientName = document.getElementById("recipientNameInput").value;
	//setting the variable to the input fields's id named hostNameInput's value
	hostName = document.getElementById("hostNameInput").value;

	//write myRecipientName to console
	console.log('Variable myRecipientName: ' + myRecipientName);
	//write hostNme to console
	console.log('Variable hostName: ' + hostName);
	
	//setting the HTML code in the span id recipientNamePlaceholder with the variable 
	document.getElementById("recipientNamePlaceholder").innerHTML = myRecipientName;
	//setting the HTML code in the span id hostnamePlaceholder with the variable
	document.getElementById("hostNamePlaceholder").innerHTML = hostName;
} 