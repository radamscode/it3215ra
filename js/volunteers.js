var $ = function (id) { return document.getElementById(id); };

var volunteerArray = [];

var displayVolunteers = function () {   
    // display the volunteers in the text area
    //$("volunteerList").value = volunteerArray.join("\n");
	// comment out the line above change this to a loop instead to loop through the array.

    //set volunteerList variable
    var volunteerList = "";
    //set arrayLength variable to the length of the volunteerArray
    var arrayLength = volunteerArray.length;
    //for loop to loop through volunteerArray
    for (let i = 0; i < arrayLength; i++ ) {

        //append volunteerList variable with current index of volunteerArray
        volunteerList += volunteerArray[i] + "\n";
        //output volunteerList to the DOM
        document.getElementById("volunteerList").innerHTML = volunteerList;
    }
	
    //write volunteerList to console
    console.log(volunteerList);
	
};

var addVolunteer = function () {
    // get the data from the form
    var volunteerString = $("first_name").value + " " + $("last_name").value;

    // store the data in an array
    volunteerArray.push(volunteerString);
    
    // display the volunteers and clear the add form
    displayVolunteers();
    
    // get the add form ready for next entry
    $("first_name").value = "";
    $("last_name").value = "";
    $("first_name").focus();
};


var deleteVolunteer = function () {
    // get the data from the form (hint: use the same format as from the add).
    // get the data from the form
    var volunteerString = $("first_name").value + " " + $("last_name").value;
    //get the index of the volunteerString in the variable
    var index = volunteerArray.indexOf(volunteerString);

    // remove the string from the array (hint, loop through the entire list, compare the string with the item in the array.
    /*
    if indexOf method does not find volunteerString in volunteerArray -1 is returned.  
    if volunteerString is found in volunteerArray, the index is returned.
    */
    if (index > -1) {
        //use splice method to removed one item in the volunteerArray starting at index.
        volunteerArray.splice(index, 1);
    }
    
    //write volunteerList to console
    console.log(volunteerList);

    // display the volunteers and clear the add form
    displayVolunteers();
    
    // get the delete form ready for next entry
    $("first_name").value = "";
    $("last_name").value = "";
    $("first_name").focus();
};

var clearList = function () {   
    // delete the data from the arrays
    volunteerArray = [];
    
	//   alternative way to delete all of the data from the array
	//    volunteerArray.length = 0;
    
    // remove the volunteers data from the web page
    $("volunteerList").value = "";
    
    $("first_name").focus();
};

var sortList = function () {   
    // sort the scores
    volunteerArray.sort();
    
    // display the scores
    displayVolunteers();    
};

//When the page is fully loaded, the buttons will be mapped to the JavaScript functions
window.onload = function () {
    $("add_button").onclick = addVolunteer;
	$("delete_button").onclick = deleteVolunteer;
    $("clear_button").onclick = clearList;    
    $("sort_button").onclick = sortList;    
    $("first_name").focus();
};