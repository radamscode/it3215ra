//this onload listener will run this code on page load
window.onload = rotate;

var theAd = 0;
//this array is used for the rotated images
var adImages = new Array("images/banner1.jpg","images/banner2.jpg","images/banner3.jpg");

//this function rotates the banner images
function rotate() {
     theAd++;
     if (theAd == adImages.length) {
        theAd = 0;
     }
     document.getElementById("Banner").src = adImages[theAd];

     setTimeout(rotate, 3 * 1000);
}