//this function will preload the banner images
function preloadImage(banner){
    const img = new Image();
    img.src = banner;
    return img
  }
  
  function preloadImages() {
    const bannerImages = []
    for (var i = 0; i < arguments.length; i++) {
        bannerImages[i] = preloadImage(arguments[i])
    }
    return bannerImages
  }
  
    //usage
  const images = preloadImages(
    "images/banner1.jpg",
    "images/banner1.jpg",
    "images/banner1.jpg"
  )