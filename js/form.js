//for validation to check if all required fields are filled out.

function checkForm(){

    var checkFrom = 0;
    var checkUsername = document.pageForm.userName.value;
    var checkPassword = document.pageForm.password.value;
    var checkPasswordVerify = document.pageForm.passwordVerify.value;
    var checkFirstName = document.pageForm.firstName.value;
    var checkLastName = document.pageForm.lastName.value;
    var checkEmail = document.pageForm.email.value;
    var checkPhoneNumber = document.pageForm.phoneNumber.value;

    form.addEventListener("submit", (e) => {

        if (checkUsername === "" || checkUsername == null) {

            document.getElementsByName("userName").innerHTML = "Your username is required.";
            checkFrom = 0;

        }else {

            document.getElementsByName("username").innerHTML = "";
            checkFrom++;
        
        }

        if (checkPassword == "") {

            document.getElementsByName("password").innerHTML = "Your password is required.";
            checkFrom = 0;
        
        }else {

            document.getElementsByName("password").innerHTML = "";
            checkFrom++;
        
        }

        if (checkPasswordVerify == "") {

            document.getElementsByName("passwordVerify").innerHTML = "Your verified password is required.";
            checkFrom = 0;
        
        }else {

            document.getElementsByName("passwordVerify").innerHTML = "";
            checkFrom++;
        
        }

        if (checkFirstName == "") {

            document.getElementsByName("firstName").innerHTML = "Your first name is required.";
            checkFrom = 0;
        
        }else {

            document.getElementsByName("firstName").innerHTML = "";
            checkFrom++;
        
        }

        if (checkLastName == "") {

            document.getElementsByName("lastName").innerHTML = "Your last name is required.";
            checkFrom = 0;
        
        }else {

            document.getElementsByName("lastName").innerHTML = "";
            checkFrom++;
        
        }

        if (checkEmail == "") {

            document.getElementsByName("email").innerHTML = "Your email is required.";
            checkFrom = 0;
        
        }else {

            document.getElementsByName("email").innerHTML = "";
            checkFrom++;
        
        }

        if (checkPhoneNumber == "") {

            document.getElementsByName("phoneNumber").innerHTML = "Your phoneNumber is required.";
            checkFrom = 0;
        
        }else {

            document.getElementsByName("phoneNumber").innerHTML = "";
            checkFrom++;
        
        }

        if (checkFrom < 7) {
            
            e.preventDefault()
            
        }
    })

    if (checkFrom == 7) {

        window.location.href = "confirm.html";
    
    }

}